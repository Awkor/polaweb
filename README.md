<div align="center">
  <img width="128" src="./static/favicon.svg">
  <h1>Polaweb</h1>
  A website change detector
</div>

## Similar projects

- [changedetection.io](https://github.com/dgtlmoon/changedetection.io)
- [Kibitzr](https://github.com/kibitzr/kibitzr)
- [Klaxon](https://github.com/themarshallproject/klaxon)
- [urlwatch](https://github.com/thp/urlwatch)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
