import difflib
import json
from typing import Union
from uuid import UUID

from fastapi import FastAPI, Header, Request
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from rich.pretty import pprint

import api
import matrix

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
app.include_router(api.router)

templates = Jinja2Templates(directory="templates")


@app.get("/", response_class=HTMLResponse)
async def get_root(
    request: Request, hx_request: Union[bool, None] = Header(default=None)
):
    return await list_targets(request, hx_request)


@app.get("/targets", response_class=HTMLResponse)
async def list_targets(
    request: Request,
    hx_request: Union[bool, None] = Header(default=None),
):
    targets = await api.list_targets(ordered=True)
    if hx_request:
        return templates.TemplateResponse(
            "targets.html", {"request": request, "targets": targets}
        )
    else:
        return templates.TemplateResponse(
            "index.html",
            {"request": request, "targets": targets, "content": "targets.html"},
        )


@app.get("/targets/{uuid}/difference", response_class=HTMLResponse)
async def get_difference(
    request: Request, uuid: UUID, hx_request: Union[bool, None] = Header(default=None)
):
    target = await api.get_target(uuid)

    change_date = target["change_date"]
    snapshot = api.get_previous_snapshot(uuid, require_content=True, date=change_date)
    from_content = snapshot["content"].splitlines()

    last_snapshot = api.get_last_snapshot(uuid, require_content=True)
    to_content = last_snapshot["content"].splitlines()

    delta = difflib.unified_diff(
        from_content,
        to_content,
        fromfile=snapshot["hash"].hex(),
        tofile=last_snapshot["hash"].hex(),
        lineterm="",
    )
    delta = "\n".join(delta)

    if hx_request:
        return templates.TemplateResponse(
            "difference.html", {"request": request, "target": target, "delta": delta}
        )
    else:
        return templates.TemplateResponse(
            "index.html",
            {
                "request": request,
                "target": target,
                "delta": delta,
                "content": "difference.html",
            },
        )


@app.delete("/targets/{uuid}/change-date", response_class=HTMLResponse)
async def delete_target_change_date(request: Request, uuid: UUID):
    await api.delete_target_change_date(uuid)
    target = await api.get_target(uuid)
    return templates.TemplateResponse(
        "target.html", {"request": request, "target": target}
    )


@app.get("/targets/{uuid}/snapshots", response_class=HTMLResponse)
async def list_snapshots(
    request: Request,
    uuid: UUID,
    hx_request: Union[bool, None] = Header(default=None),
):
    target = await api.get_target(uuid)
    snapshots = await api.list_snapshots(uuid)
    # Convert rows to dictionaries to make them editable.
    snapshots = [dict(snapshot) for snapshot in snapshots]

    for snapshot in snapshots:
        snapshot["hash"] = snapshot["hash"].hex()

    if hx_request:
        return templates.TemplateResponse(
            "snapshots.html",
            {"request": request, "snapshots": snapshots, "target": target},
        )
    else:
        return templates.TemplateResponse(
            "index.html",
            {
                "request": request,
                "snapshots": snapshots,
                "target": target,
                "content": "snapshots.html",
            },
        )


@app.on_event("startup")
async def startup_event():
    targets = await api.list_targets()
    # Add jobs for existing targets.
    for target in targets:
        uuid = target["uuid"]
        api.add_target_job(uuid)
    # Import targets from targets.json file.
    urls = [target["url"] for target in targets]
    with open("targets.json") as targets_file:
        targets_from_file = json.load(targets_file)
        for target_from_file in targets_from_file:
            url = target_from_file["url"]
            if url not in urls:
                selector = target_from_file["selector"]
                target = api.Target(selector=selector, url=url)
                await api.add_target(target)

    await matrix.setup()


@app.on_event("shutdown")
async def shutdown_event():
    await matrix.client.close()
