import sqlite3
from datetime import datetime
from typing import Union
from urllib.parse import urlparse
from uuid import UUID, uuid4

import aiohttp
import html2text
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from blake3 import blake3
from bs4 import BeautifulSoup
from fastapi import APIRouter, Body, Response, status
from pydantic import BaseModel
from rich.console import Console

import matrix

console = Console()

sqlite3.register_adapter(UUID, lambda uuid: uuid.bytes)
sqlite3.register_converter("UUID", lambda uuid_bytes: UUID(bytes=uuid_bytes))

connection = sqlite3.connect("database.sqlite3", detect_types=sqlite3.PARSE_DECLTYPES)
connection.row_factory = sqlite3.Row

router = APIRouter(prefix="/api")

scheduler = AsyncIOScheduler()
scheduler.start()

connection.execute(
    """
    CREATE TABLE IF NOT EXISTS Target(
               uuid  UUID   NOT NULL PRIMARY KEY,
                url  TEXT   NOT NULL,
           selector  TEXT   NOT NULL,
        change_date INTEGER,
        UNIQUE(url, selector)
    )
    """
)

connection.execute(
    """
    CREATE TABLE IF NOT EXISTS Snapshot(
               date INTEGER NOT NULL,
               hash  BLOB,
            content  TEXT,
        target_uuid  UUID   NOT NULL,
        PRIMARY KEY(date, target_uuid),
        FOREIGN KEY(target_uuid) REFERENCES Target(uuid)
    )
    """
)

connection.execute(
    """
    CREATE VIEW IF NOT EXISTS SnapshotWithContent(date, hash, content, target_uuid) AS
    SELECT date, hash, content, target_uuid
      FROM Snapshot
     WHERE content NOT NULL
    """
)

# NOTE: We use response_class=Response to return an empty response instead of "null".


@router.get("/targets")
async def list_targets(ordered: bool = False):
    query = """
        SELECT change_date, uuid, url, selector
          FROM Target
    """
    if ordered:
        query += "ORDER BY change_date DESC, url ASC"
    return connection.execute(query).fetchall()


class Target(BaseModel):
    selector: str
    url: str


@router.get("/targets/{uuid}")
async def get_target(uuid: UUID):
    return connection.execute(
        """
        SELECT change_date, selector, uuid, url
          FROM Target
         WHERE uuid = :uuid
        """,
        {"uuid": uuid},
    ).fetchone()


@router.post("/targets", status_code=status.HTTP_201_CREATED)
async def add_target(target: Target):
    uuid = uuid4()
    connection.execute(
        """
        INSERT INTO Target(
            change_date,
            selector,
            url,
            uuid
        ) VALUES (
            NULL,
            :selector,
            :url,
            :uuid
        )
        """,
        {"selector": target.selector, "url": target.url, "uuid": uuid},
    )
    connection.commit()
    add_target_job(uuid)


@router.delete("/targets/{uuid}", response_class=Response)
async def delete_target(uuid: UUID):
    connection.execute(
        """
        DELETE
          FROM Snapshot
         WHERE target_uuid = :target_uuid
        """,
        {"target_uuid": uuid},
    )
    connection.execute(
        """
        DELETE
          FROM Target
         WHERE uuid = :uuid
        """,
        {"uuid": uuid},
    )
    connection.commit()
    delete_target_job(uuid)


@router.delete("/targets/{uuid}/change-date", response_class=Response)
async def delete_target_change_date(uuid: UUID):
    await set_target_change_date(uuid, None)


async def set_target_change_date(uuid: UUID, change_date: Union[int, None]):
    connection.execute(
        """
        UPDATE Target
           SET change_date = :change_date
         WHERE uuid = :uuid
        """,
        {"change_date": change_date, "uuid": uuid},
    )
    connection.commit()


@router.get("/targets/{uuid}/snapshots")
async def list_snapshots(uuid: UUID):
    return connection.execute(
        """
          SELECT content, date, hash, target_uuid
            FROM Snapshot
           WHERE target_uuid = :target_uuid
        ORDER BY date DESC
        """,
        {"target_uuid": uuid},
    ).fetchall()


@router.post("/targets/{uuid}/snapshots")
async def add_snapshot(uuid: UUID):
    target = await get_target(uuid)

    url = target["url"]
    html = await get_html(url)
    soup = BeautifulSoup(html, "lxml")
    soup_selection = soup.select(target["selector"])
    if len(soup_selection) == 0:
        console.log("Selection of target", uuid, "returned empty")
        console.log(html)
        return
    soup_selection = soup_selection[0]

    text = str(soup_selection)
    text = html2text.html2text(text)
    text_bytes = text.encode()
    snapshot_hash = blake3(text_bytes).digest()

    now = datetime.now().timestamp()
    now = int(now)

    last_snapshot = get_last_snapshot(uuid, require_content=False)
    if last_snapshot:
        if snapshot_hash == last_snapshot["hash"]:
            # We don't need to save the content since it didn't change.
            text = None
        elif target["change_date"] is None:
            await set_target_change_date(uuid, now)
            # Notify user that the target changed.
            await matrix.send_message(matrix.CLIENT_ROOM, f"Target {url} changed.")

            console.log("Target", uuid, "changed")

    connection.execute(
        """
        INSERT INTO Snapshot(content, date, hash, target_uuid)
        VALUES (:content, :date, :hash, :target_uuid)
        """,
        {
            "content": text,
            "date": now,
            "hash": snapshot_hash,
            "target_uuid": uuid,
        },
    )
    connection.commit()
    console.log("Added snapshot of target", uuid)


@router.get("/targets/{uuid}/snapshots/{date}")
async def get_snapshot(uuid: UUID, date: int):
    return connection.execute(
        """
        SELECT content, date, hash, target_uuid
          FROM Snapshot
         WHERE date = :date
           AND target_uuid = :target_uuid
        """,
        {"date": date, "target_uuid": uuid},
    ).fetchone()


async def get_html(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            return await response.text()


def add_target_job(uuid: UUID):
    uuid_string = str(uuid)
    scheduler.add_job(
        add_snapshot, "interval", [uuid], id=uuid_string, hours=1, jitter=(60 * 60 * 2)
    )
    console.log("Added job for target", uuid)


def delete_target_job(uuid: UUID):
    uuid_string = str(uuid)
    scheduler.remove_job(job_id=uuid_string)
    console.log("Deleted job for target", uuid)


def get_last_snapshot(uuid: UUID, require_content: bool):
    table = "SnapshotWithContent" if require_content else "Snapshot"
    return connection.execute(
        f"""
          SELECT content, date, hash, target_uuid
            FROM {table}
           WHERE target_uuid = :target_uuid
        ORDER BY date DESC
           LIMIT 1
        """,
        {
            "target_uuid": uuid,
        },
    ).fetchone()


def get_previous_snapshot(uuid: UUID, require_content: bool, date: int):
    table = "SnapshotWithContent" if require_content else "Snapshot"
    return connection.execute(
        f"""
          SELECT content, date, hash, target_uuid
            FROM {table}
           WHERE target_uuid = :target_uuid
             AND date < :date
        ORDER BY date DESC
           LIMIT 1
        """,
        {
            "date": date,
            "target_uuid": uuid,
        },
    ).fetchone()
