import json
from datetime import timedelta

from nio import AsyncClient, MatrixRoom
from nio.event_builders import (
    ChangeGuestAccessBuilder,
    ChangeHistoryVisibilityBuilder,
    EnableEncryptionBuilder,
)
from nio.events.room_events import RoomMessageText

client = None

settings_file = open("settings.json")
settings = json.load(settings_file)

CLIENT_HOME_SERVER = settings["client"]["home_server"]
CLIENT_PASSWORD = settings["client"]["password"]
CLIENT_ROOM = settings["client"]["room"]
CLIENT_USER_ID = settings["client"]["user_id"]
RECEIVER_USER_ID = settings["receiver"]["user_id"]

del settings
del settings_file


async def leave_empty_rooms():
    joined_rooms_response = await client.joined_rooms()
    for room_id in joined_rooms_response.rooms:
        joined_members_response = await client.joined_members(room_id)
        if len(joined_members_response.members) == 1:
            await client.room_leave(room_id)


async def get_all_room_messages(room_id):
    messages_per_request = 10
    start_token = str()
    messages = []
    while True:
        response = await client.room_messages(room_id, start_token)
        start_token = response.end
        messages_in_response = response.chunk
        messages = messages + messages_in_response
        if len(messages_in_response) < messages_per_request:
            break
    return messages


async def remove_old_messages():
    joined_rooms_response = await client.joined_rooms()
    for room_id in joined_rooms_response.rooms:
        room_messages = await get_all_room_messages(room_id)
        room_messages = list(
            filter(lambda message: isinstance(message, RoomMessageText), room_messages)
        )
        for room_message in room_messages:
            message_age = room_message.source["age"]
            if timedelta(milliseconds=message_age).days > 0:
                await client.room_redact(room_id, room_message.event_id)


async def setup():
    global client
    client = AsyncClient(CLIENT_HOME_SERVER, CLIENT_USER_ID)
    await client.login(CLIENT_PASSWORD)
    await remove_old_messages()
    await leave_empty_rooms()
    # TODO:
    # if not await hermes.is_in_room_with(RECEIVER_USER_ID):
    #     await hermes.create_direct_room(RECEIVER_USER_ID)


async def send_message(room_id: str, message: str):
    await client.room_send(
        content={"body": message, "msgtype": "m.text"},
        message_type="m.room.message",
        room_id=room_id,
    )


async def create_direct_room(user_id):
    await client.room_create(
        initial_state=[
            ChangeGuestAccessBuilder("forbidden").as_dict(),
            ChangeHistoryVisibilityBuilder("invited").as_dict(),
            EnableEncryptionBuilder().as_dict(),
        ],
        invite=[user_id],
        is_direct=True,
        name="Polaweb",
    )
